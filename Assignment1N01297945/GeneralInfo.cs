﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1N01297945
{
    public class GeneralInfo
    {
        private string GeneralInfoCustomerName;
        private string GeneralInfoCustomerEmail;
        private string GeneralInfoCustPhoneNo;
        private string GeneralInfoCustAddress;
        
        public string CustomerName1
        {
            get { return GeneralInfoCustomerName; }
            set { GeneralInfoCustomerName = value; }
        }
        public string CustomerEmail1
        {
            get { return GeneralInfoCustomerEmail; }
            set { GeneralInfoCustomerEmail = value; }
        }
        public string CustPhoneNo1
        {
            get { return GeneralInfoCustPhoneNo; }
            set { GeneralInfoCustPhoneNo = value; }
        }
        public string CustAddress1
        {
            get { return GeneralInfoCustAddress; }
            set { GeneralInfoCustAddress = value; }
        }
    }
}

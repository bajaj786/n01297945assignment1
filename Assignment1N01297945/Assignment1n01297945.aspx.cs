﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1N01297945
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void order(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }

            //general info
            string name = CustomerName.Text.ToString();
            string email = CustomerEmail.Text.ToString();
            string phone = CustPhoneNo.Text.ToString();
            GeneralInfo newcustomer = new GeneralInfo();
            newcustomer.CustomerName1 = name;
            newcustomer.CustPhoneNo1 = phone;
            newcustomer.CustomerEmail1 = email;
            
            //Meal Delivery
            string address = CustAddress.Text.ToString();
            MealDelivery md = new MealDelivery(address);
           
            
            //Menu Selection
            string MealTime = mealtime.SelectedItem.Text.ToString();
         



            List<string> foodtypeselected = new List<string>();

            foreach (Control control in foodtype.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox bajaj = (CheckBox)control;
                    if (bajaj.Checked)
                    {
                        foodtypeselected.Add(bajaj.Text);
                    }

                }
      
            }




            MenuSelection mt = new MenuSelection(MealTime,foodtypeselected);

            MasterClass obj = new MasterClass(newcustomer, mt, md);

            result1.InnerHtml = obj.Invoice();
        }
    }
}
        

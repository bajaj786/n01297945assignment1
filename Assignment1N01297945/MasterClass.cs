﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1N01297945
{
    public class MasterClass
    {

        public GeneralInfo GInfo;
        public MenuSelection mSelection;
        public MealDelivery mDelivery;

        public MasterClass(GeneralInfo GI, MenuSelection MS, MealDelivery MD)
        {
            GInfo = GI;
            mSelection = MS;
            mDelivery = MD;

        }

        

        public string Invoice()
        {
           
                //general info
                string Bill = "Invoice:<br>";
                Bill += "Your total is :" + OrderCalculation().ToString() + "<br/>";
                Bill += "Name: " + GInfo.CustomerName1 + "<br/>";
                Bill += "Email: " + GInfo.CustomerEmail1 + "<br/>";
                Bill += "Phone Number: " + GInfo.CustPhoneNo1 + "<br/>";
                Bill += "CustomerAddress: " + mDelivery.CustAddress + "<br/>";
                //list,checkbox
                Bill += "Meal Time: " + mSelection.mealTime + "<br/>";
               // Bill += "Veggie Food: " + mSelection.foodtype + "<br/>";
                //Bill += "Non-Veggie Food: " + mSelection.foodtype + "<br/>";

                Bill += "Deliver this order to : " + mDelivery.CustAddress + "<br/>";

            return Bill;
            
        }


        public double OrderCalculation()
        {
            double total = 10;
            
            total += mSelection.foodtype.Count() * 2.00;

            return total;
        }
    }

}
﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1N01297945.aspx.cs" Inherits="Assignment1N01297945.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Ordering a Meal from Menu</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
            <h1>Ordering a Meal from Menu</h1>
            Name:<asp:TextBox runat="server" ID="CustomerName" placeholder="e.g John foote" ></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Full Name" ControlToValidate="CustomerName" ID="validatorName"></asp:RequiredFieldValidator>
            <br />
            E-mail:<asp:TextBox runat="server" ID="CustomerEmail" placeholder="e.g abc@def.com"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your E-mail id" ControlToValidate="CustomerEmail" ID="RequiredFieldValidatorss"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="CustomerEmail" ErrorMessage="Email format is wrong"></asp:RegularExpressionValidator>
       <br />
            Phone No.:<asp:TextBox runat="server" ID="CustPhoneNo" placeholder="e.g 0123456789"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Phone No." ControlToValidate="CustPhoneNo" ID="RequiredFieldValidators"></asp:RequiredFieldValidator>
            <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Phone No. is Incorrect" ControlToValidate="CustPhoneNo" ValidationExpression="[0-9]{}" ></asp:RegularExpressionValidator>--%>
        <br/>
            Address:<asp:TextBox runat="server" ID="CustAddress" placeholder="e.g 123 ABC st, A1A1A1"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Enter Your Address" ControlToValidate="CustAddress" ID="RequiredFieldValidator"></asp:RequiredFieldValidator>
            

            </div>
            Type Of Food :<div id="TypeOfFood" runat="server">
            <asp:RadioButton runat="server" Text="Vegetarian" />
            <asp:RadioButton runat="server" Text="Non-Vegetarian"/>
               
                <div id="foodtype" runat="server">
                 <asp:CheckBox runat="server" ID="CheckBox1" Text="Grilled cheese panneni" />
                 <asp:CheckBox runat="server" ID="CheckBox2" Text="Everything Bagel" />
                 <asp:CheckBox runat="server" ID="CheckBox3" Text="Veggie Sandwich" />
                 <asp:CheckBox runat="server" ID="CheckBox4" Text="Veggie Wrap" />
                 <asp:CheckBox runat="server" ID="CheckBox5" Text="Egg & Cheese (Home style Biscuit)" />
                 <asp:CheckBox runat="server" ID="CheckBox6" Text="Sausage" />
                 <asp:CheckBox runat="server" ID="CheckBox7" Text="Butter Chicken" />
                <asp:CheckBox runat="server"  ID="CheckBox8" Text="Baecon Sandwich" />
                </div>
        </div>
        <div>
             Meal-Time:<asp:DropDownList runat="server" ID="mealtime">
             <asp:ListItem Value="breakfast" Text="9:00 AM-12:00 PM"></asp:ListItem>
             <asp:ListItem Value="lunch" Text="12:00 PM-4:30 PM"></asp:ListItem>
             <asp:ListItem Value="dinner" Text="5:00 PM-10:00 PM"></asp:ListItem>
            </asp:DropDownList>
        </div>
            
            
           <br />
        <asp:ValidationSummary ID="VS"  runat="server" DisplayMode="BulletList" EnableClientScript="true" HeaderText="Complete all fields"/>
        <br/>
                <asp:button runat="server" ID="enterdata" Text="Submit" OnClick="order"/>
        <div runat="server" id="result1">



        </div>
        
            </form>
      </body>
</html>
